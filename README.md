# React Leaflet Map - PART I

It's a tutorial for beginners, who want to use Leaflet maps with React.


### Medium Atricle by Me:
**Link:** https://javascript.plainenglish.io/leaflet-map-with-react-part-i-4ef4ecbdcc1b


### Steps to create the project from scratch:
1. Create your Project using create react app.  
   `npx create-react-app react-leaflet-map`  
   `cd react-leaflet-map` 


2. Add the additional packages for leaflet map with react  
   `yarn add leaflet react-leaflet@2.7.0`
  

3. Start the project.  
   `yarn start`

![React with Leaflet Map](src/assets/readme-react-leaflet-map.png "React with Leaflet Map")

### Packages Used: 
1. [leaflet](https://www.npmjs.com/package/leaflet)
2. [react-leaflet@2.7.0](https://www.npmjs.com/package/react-leaflet/v/2.7.0)  
   **NOTE: Please make sure you are using version `2.7.0` of `react-leaflet`, because newer version have some problems.**  

### Read Other Article:  
- [27 Essential One-Line JavaScript Functions Used By Developers Daily](https://javascript.plainenglish.io/27-essential-one-line-javascript-functions-used-by-developers-daily-2cda9826700e)  
- [3 Uses of ‘?’ in JavaScript❓](https://javascript.plainenglish.io/3-uses-of-in-javascript-why-pro-developers-love-using-javascript-operator-565bc8b235a4)  
- [10 Must-Have Chrome Extensions for Web Developers](https://javascript.plainenglish.io/10-must-have-chrome-extensions-for-web-developers-ed40e3e1081f)
