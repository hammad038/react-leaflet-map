import React, { Fragment } from "react";
import L from 'leaflet';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import './App.css';
import { venues } from './assets/data.json';
import markerIcon from './assets/icon.svg';

// Map default options.
const options = {
  currentLocation: { lat: 52.52437, lng: 13.41053 },
  zoom: 12,
}

// Create marker icon.
const VenueLocationIcon = L.icon({
    iconUrl: markerIcon,
    iconSize: [35, 35],
});

// React App Map View.
const App = () =>
    <Map center={options.currentLocation} zoom={options.zoom}>
        <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
        />
        <VenueMarkers />
    </Map>

export { App as default };

// All venue markers.
export const VenueMarkers = () => {
    const markers = venues.map((venue, venueCount) => (
        <Marker key={venueCount} position={venue.geometry} icon={VenueLocationIcon}>
            <MarkerPopup name={venue.name} description={venue.description}/>
        </Marker>
    ));

    return <Fragment>{markers}</Fragment>
};

// Popup for markers with Venue name and description.
const MarkerPopup = ({name, description}) =>
    <Popup>
        <h3>{name}</h3>
        <p>{description}</p>
    </Popup>
